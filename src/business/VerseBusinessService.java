package business;

import entity.BibleVerse;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class VerseBusinessService
 */
@Stateless
@Local(VerseBusinessInterface.class)
@LocalBean
public class VerseBusinessService implements VerseBusinessInterface {

	private List<BibleVerse> bibleVerses;
	
    /**
     * Default constructor. 
     * Initialize bibleVerses List<BibleVerse>
     */
    public VerseBusinessService() {
    	BibleVerse[] verses = new BibleVerse[] {
				new BibleVerse("The beginning of the good news about Jesus the Messiah, the Son of God", "Mark", 1, 1),
				new BibleVerse("as it is written in Isaiah the prophet: \"I will send my messenger ahead of you, who will prepare your way\"", "Mark", 1, 2),
				new BibleVerse("\"a voice of one calling in the wilderness, �Prepare the way for the Lord, make straight paths for him.� \"", "Mark", 1, 3),
				new BibleVerse("And so John the Baptist appeared in the wilderness, preaching a baptism of repentance for the forgiveness of sins.", "Mark", 1, 4),
				new BibleVerse("In the beginning God created the heavens and the earth.", "Genesis", 1, 1),
				new BibleVerse("But the angel said to her, \"Do not be afraid, Mary; you have found favor with God.\"", "Luke", 1, 30),
				new BibleVerse("\"You will conceive and give birth to a son, and you are to call him Jesus\"", "Luke", 1, 31),
				new BibleVerse("Since, then, you have been raised with Christ, set your hearts on things above, where Christ is, seated at the right hand of God.", "Colossians", 3, 1),
				new BibleVerse("\"Come, all you who are thirsty, come to the waters; and you who have no money, come, buy and eat! Come, buy wine and milk without money and without cost.\"", "Isaiah", 55, 1),
				new BibleVerse("For this reason he had to be made like them, fully human in every way, in order that he might become a merciful and faithful high priest in service to God, and that he might make atonement for the sins of the people. ", "Hebrews", 2, 17)
    	};
		
		//Initialize orders with a dummyOrder array
		bibleVerses = Stream.of(verses).collect(Collectors.toList());
    }
    
    public String getVerse(String book, int chapter, int verse) {
    	String capBook = book.substring(0, 1).toUpperCase() + book.substring(1);
    	for(BibleVerse v : bibleVerses) {
    		if(v.getBookName().equalsIgnoreCase(book) && v.getChapNum() == chapter && v.getVerseNum() == verse) {
    			return String.format("Verse Found:\n%s(%s %d:%d New International Version)", v.getVerse(), capBook, chapter, verse);
    		}
    	}
    	return String.format("I'm sorry our Bible Verse database did not find a match for your query(%s %d:%d) \nPlease Try Another Query", capBook, chapter, verse);
    }

    //--- Getters & Setters ---
	@Override
	public List<BibleVerse> getBibleVerses() {
		return bibleVerses;
	}

	@Override
	public void setBibleVerses(List<BibleVerse> bibleVerses) {
		this.bibleVerses = bibleVerses;
		
	}


}
