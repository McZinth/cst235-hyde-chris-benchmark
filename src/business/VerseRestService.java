package business;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import entity.BibleVerse;

@RequestScoped
@Path("/verses")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class VerseRestService {
	
	@Inject
	VerseBusinessInterface service;
	
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BibleVerse> getVersesAsJson(){
		return service.getBibleVerses();
	}
	
	@GET
	@Path("/verse")
	@Produces(MediaType.APPLICATION_JSON)
	public String getVerseJson(
			@DefaultValue("Mark") @QueryParam("book") String book,
			@DefaultValue("1") @QueryParam("chapter") int chapter,
			@DefaultValue("1") @QueryParam("verse") int verse
			) {
		return service.getVerse(book, chapter, verse);
	}

}
