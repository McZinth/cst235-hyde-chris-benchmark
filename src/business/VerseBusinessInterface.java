package business;

import java.util.List;

import javax.ejb.Local;

import entity.BibleVerse;

@Local()
public interface VerseBusinessInterface {
	public String getVerse(String book, int chapter, int verse);
	public List<BibleVerse> getBibleVerses();
	public void setBibleVerses(List<BibleVerse> bibleVerses);
}
