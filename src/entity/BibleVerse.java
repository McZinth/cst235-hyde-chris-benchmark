package entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Verse")
public class BibleVerse {
	private String bookName;
	private String verse;
	private int chapNum;
	private int verseNum;
	
	public BibleVerse() {
		
	}

	/**
	 * @param verse
	 * @param bookName
	 * @param chapNum
	 * @param verseNum
	 */
	public BibleVerse(String verse, String bookName, int chapNum, int verseNum) {
		this.bookName = bookName;
		this.verse = verse;
		this.chapNum = chapNum;
		this.verseNum = verseNum;
	}

	/**
	 * @return the bookName
	 */
	public String getBookName() {
		return bookName;
	}

	/**
	 * @param bookName the bookName to set
	 */
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	/**
	 * @return the verse
	 */
	public String getVerse() {
		return verse;
	}

	/**
	 * @param verse the verse to set
	 */
	public void setVerse(String verse) {
		this.verse = verse;
	}

	/**
	 * @return the chapNum
	 */
	public int getChapNum() {
		return chapNum;
	}

	/**
	 * @param chapNum the chapNum to set
	 */
	public void setChapNum(int chapNum) {
		this.chapNum = chapNum;
	}

	/**
	 * @return the verseNum
	 */
	public int getVerseNum() {
		return verseNum;
	}

	/**
	 * @param verseNum the verseNum to set
	 */
	public void setVerseNum(int verseNum) {
		this.verseNum = verseNum;
	}
	
	
	
}
